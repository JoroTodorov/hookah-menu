﻿using System.Linq;
using HookahMenu.Models;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ChangeTracking.Internal;

namespace HookahMenu.Controllers
{
    [Authorize]
    [Route("api/users")]
    public class UsersController : Controller
    {
        private readonly IUsersService service;
        private readonly UserManager<Client> um;

        public UsersController(IUsersService service, UserManager<Client> um)
        {
            this.service = service;
            this.um = um;
        }

        [Route("role")]
        public ActionResult GetRole()
        {
            var user = this.um.GetUserAsync(this.User).Result;
            var roles = this.um.GetRolesAsync(user).Result;

            return this.Ok(roles);
        }

        [Authorize]
        [Route("visit")]
        public ActionResult GetTable()
        {
            var user = this.um.GetUserAsync(this.User).Result;
            var activeVisit = this.service.GetActiveVisit(user);

            return this.Ok(activeVisit);
        }
    }
}