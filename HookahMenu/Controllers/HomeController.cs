﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HookahMenu.Controllers
{
    [Route("")]
    [Authorize]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return this.View();
        }

        [Route("tables")]
        public ActionResult Tables()
        {
            return this.View("Index");
        }
    }
}
