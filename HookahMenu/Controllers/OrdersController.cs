﻿using System;
using HookahMenu.Models;
using HookahMenu.Services;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HookahMenu.Controllers
{
    [Authorize]
    [Route("api/orders")]
    public class OrdersController : Controller
    {
        private readonly IOrdersService service;
        private readonly UserManager<Client> userManager;

        public OrdersController(IOrdersService service, UserManager<Client> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }

        [HttpPost]
        public ActionResult Get()
        {
            try
            {
                var clientId = this.userManager.GetUserId(this.User);
                var orders = this.service.GetRecentOrders(clientId);

                return this.Ok(orders);
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }
    }
}
