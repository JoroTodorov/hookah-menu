﻿using System;
using System.Linq;
using HookahMenu.Models;
using HookahMenu.Services;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HookahMenu.Controllers
{
    [Authorize]
    [Route("api/tables")]
    public class TablesController : Controller
    {
        private readonly ITablesService service;
        private readonly UserManager<Client> userManager;

        public TablesController(ITablesService service, UserManager<Client> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var tables = service.GetTables(t => !t.Visits.Any(v => v.IsActive));

            return this.Ok(tables);
        }

        [HttpPost]
        public ActionResult Post([FromBody] int tableId)
        {
            try
            {
                var clientId = this.userManager.GetUserId(this.User);
                this.service.SetTableVisit(tableId, clientId);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }
    }
}