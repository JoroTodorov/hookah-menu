﻿using System;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HookahMenu.Controllers.Admin
{
    [Authorize]
    [Route("api/admin/orders")]
    public class OrdersController : Controller
    {
        private readonly IOrdersService service;

        public OrdersController(IOrdersService service)
        {
            this.service = service;
        }

        [HttpPost]
        public ActionResult Post([FromBody]int id)
        {
            try
            {
                this.service.SetVisitAsPaid(id);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody]int id)
        {
            try
            {
                this.service.SetOrderAsFinished(id);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }
    }
}