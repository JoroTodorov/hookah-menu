﻿using System;
using System.Linq;
using HookahMenu.Services;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace HookahMenu.Controllers.Admin
{
    [Authorize]
    [Route("api/admin/tables")]
    public class TablesController : Controller
    {
        private readonly ITablesService service;

        public TablesController(ITablesService service)
        {
            this.service = service;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var tables = service.GetTables(t => t.Visits.Any(v => v.IsActive));

            return this.Ok(tables);
        }

        [HttpGet]
        [Route("{id:int}")]
        public ActionResult Get(int id)
        {
            try
            {
                var tables = service.GetTableOrders(id);

                return this.Ok(tables);
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }

        [HttpPost]
        public ActionResult Post()
        {
            this.service.AddTable();

            return this.Ok();
        }


        [HttpPut]
        public ActionResult Put([FromBody]int id)
        {
            try
            {
                this.service.FreeTable(id);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }
    }
}