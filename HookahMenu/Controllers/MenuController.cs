﻿using System;
using System.Linq;
using System.Net;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;
using HookahMenu.Services;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HookahMenu.Controllers
{
    [Authorize]
    [Route("api/menu")]
    public class MenuController : Controller
    {
        private readonly IMenuService service;
        private readonly UserManager<Client> userManager;

        public MenuController(IMenuService service, UserManager<Client> userManager)
        {
            this.service = service;
            this.userManager = userManager;
        }

        [HttpGet]
        public ActionResult Get()
        {
            var menu = this.service.GetMenu();

            return this.Ok(menu);
        }

        [HttpPost]
        public ActionResult Post([FromBody] OrderDTO order)
        {
            try
            {
                var clientId = this.userManager.GetUserId(this.User);
                this.service.OrderHookah(order, clientId);

                return this.Ok();
            }
            catch (Exception ex)
            {
                return this.BadRequest(ex);
            }
        }
    }
}