define(['../../core/remote.js', '../../core/utils.js'], function (remote, utils) {
    function displayActiveTables(ctx) {
        let templates = {
            content: '/temp/admin/tables.hbs'
        };

        remote.get('admin/tables').then(function (data) {
            ctx.tables = data;
            utils.loadPage(ctx, templates);
        });
    }

    function displayTableOrders(ctx) {
        let templates = {
            content: '/temp/admin/details.hbs',
            tobacco: '/temp/list/tobacco.hbs',
            addon: '/temp/list/addon.hbs'
        };

        remote.get('admin/tables/' + ctx.params.id).then(function (data) {
            ctx.tableId = ctx.params.id;
            ctx.id = data.id;
            ctx.price = data.price;
            ctx.orders = data.orders;

            utils.loadPage(ctx, templates);
        });
    }

    function freeTable(ctx) {
        remote.update('admin/tables', ctx.params.id).then(function () {
            ctx.redirect('/admin/tables');
            utils.showInfo("Table is open.");
        }).catch(function(ex){
            utils.showError(ex);         
        });
    }

    function addTable(ctx) {
        remote.post('admin/tables').then(function () {
            ctx.redirect('/admin');
            utils.showInfo("Table added.")
        });
    }

    return {
        addTable,
        displayActiveTables,
        displayTableOrders,
        freeTable
    }
});