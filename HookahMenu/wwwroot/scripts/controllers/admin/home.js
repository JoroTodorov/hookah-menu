define(['../../core/utils.js'], function (utils) {
    function displayIndex(ctx) {
        let templates = {
            content: '/temp/admin/index.hbs'
        };
        utils.loadPage(ctx, templates);
    }

    return {
        displayIndex
    }
});