define(['../../core/remote.js', '../../core/utils.js'], function (remote, utils) {
    function finishOrder(ctx){
        remote.update('admin/orders', ctx.params.id).then(function () {
            ctx.redirect('/admin/tables/' + ctx.params.tableId);
            utils.showInfo("Order is finished.");
        }).catch(function(ex){
            utils.showError(ex);         
        });
    }

    function finishPayment(ctx){
        remote.post('admin/orders', ctx.params.id).then(function () {
            ctx.redirect('/admin/tables');
            utils.showInfo("Visit set as paid.");
        }).catch(function(ex){
            utils.showError(ex);         
        });
    }

    return {
        finishOrder,
        finishPayment
    }
});