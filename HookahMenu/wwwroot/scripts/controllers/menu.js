define(['../core/remote.js', '../core/utils.js'], function (remote, utils) {
    $(document).on("click", ".img-tobacco", selectItem);

    function displayOrder(ctx) {
        let templates = {
            content: '/temp/menu/order.hbs',
            addon: '/temp/menu/addon.hbs',
            tobacco: '/temp/menu/tobacco.hbs'
        };

        remote.get('menu').then(function (data) {
            ctx.tobaccos = data.tobaccos;
            ctx.addons = data.addons;
            utils.loadPage(ctx, templates);
        });
    }

    function handleOrder(ctx) {
        let tobaccos = ctx.params.flavours;
        tobaccos = tobaccos === undefined ? "0" : tobaccos;
        tobaccos = tobaccos.length === 1 ? [tobaccos] : tobaccos;

        let addons = ctx.params.addons;
        addons = addons === undefined ? "0" : addons;
        addons = addons.length === 1 ? [addons] : addons;

        let data = {
            tobaccos,
            addons
        };

        remote.post('menu', data).then(function() {
            ctx.redirect('/');
            utils.showInfo("Successfully ordered.");
        }).catch(function(ex) {
            ctx.redirect('/menu');
            utils.showError(ex);
        });
    }

    function selectItem() {
        let isChecked = $(this).attr("class")
            .split(' ').includes("checked");
        isChecked ?
            $(this).removeClass("checked") : $(this).addClass("checked");;
    }

    return {
        displayOrder,
        handleOrder
    }
});