define(['../core/utils.js', '../core/remote.js'], function (utils, remote) {
    function displayIndex(ctx) {
        remote.get('users/visit').then(function (visitData) {
            if(visitData === undefined){
                ctx.redirect('/tables');
                return;
            }
            remote.get('users/role').then(function (roleData) {
                //for testing purposes every user is an admin
                //ctx.isAdmin = data.includes("Admin");;
                ctx.isAdmin = true;

                let templates = {
                    content: '/temp/index.hbs'
                };

                utils.loadPage(ctx, templates);
            });
        });
    }

    return {
        displayIndex
    }
});