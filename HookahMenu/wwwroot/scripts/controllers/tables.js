define(['../core/remote.js', '../core/utils.js'], function (remote, utils) {
    function displayTables(ctx) {
        let templates = {
            content: '/temp/tables/index.hbs'
        };

        remote.get('tables').then(function (data) {
            remote.get('users/role').then(function (roleData) {
                //for testing purposes every user is an admin
                //ctx.isAdmin = data.includes("Admin");;
                ctx.isAdmin = true;
                ctx.tables = data;

                utils.loadPage(ctx, templates);
            });
        });
    }

    function reserveTable(ctx) {
        remote.post('tables', ctx.params.id).then(function () {
            ctx.redirect('/');
            utils.showInfo("Table chosen.");
        }).catch(function (ex) {
            ctx.redirect("/tables");
            utils.showError(ex);
        });
    }

    return {
        displayTables,
        reserveTable
    }
});