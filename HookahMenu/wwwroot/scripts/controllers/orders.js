define(['../core/remote.js', '../core/utils.js'], function (remote, utils) {
    function displayRecentOrders(ctx) {
        let templates = {
            content: '/temp/list/list.hbs',
            addon: '/temp/list/addon.hbs',
            tobacco: '/temp/list/tobacco.hbs'
        };

        remote.post('orders')
            .then(function (data) {
                ctx.hookahs = data;
                utils.loadPage(ctx, templates);
            });
    }

    return {
        displayRecentOrders
    }
});