(() => {
    define(['./scripts/controllers/home.js', './scripts/controllers/menu.js',
        './scripts/controllers/orders.js', './scripts/controllers/tables.js', 
        './scripts/controllers/admin/home.js',
        './scripts/controllers/admin/tables.js',
        './scripts/controllers/admin/orders.js'],
        function (home, menu, orders, tables, admin, admTables, admOrders) {
            const app = Sammy('#main', function () {
                this.use('Handlebars', 'hbs');

                //index
                this.get('/', home.displayIndex);

                //admin
                this.get('/admin', admin.displayIndex);

                this.get('/admin/tables/add', admTables.addTable);
                this.get('/admin/tables', admTables.displayActiveTables);

                this.get('/admin/tables/:id', admTables.displayTableOrders);
                this.post('/admin/tables/:id', admTables.freeTable);

                this.get('/admin/orders/:id', admOrders.finishPayment);
                this.post('/admin/orders/:id', admOrders.finishOrder);

                //tables
                this.get('/tables/:id', tables.reserveTable);
                this.get('/tables', tables.displayTables);

                //menu
                this.get('/menu', menu.displayOrder);
                this.post('/menu', menu.handleOrder);

                //orders
                this.get('/orders', orders.displayRecentOrders);
            });

            if (window.location.hash && window.location.hash === '#_=_') {
                window.location.hash = '';
            }
            app.run();
        });
})();
