define(function () {
    function makeRequest(method, url) {
        return {
            url: `/api/${url}`,
            method,
            contentType: 'application/json'
        };
    }

    function get(url) {
        let req = makeRequest('GET', url);
        
        return $.ajax(req);
    }

    function post(url, data) {
        let req = makeRequest('POST', url);
        req.data = JSON.stringify(data);

        return $.ajax(req);
    }

    function update(url, data) {
        let req = makeRequest('PUT', url);
        req.data = JSON.stringify(data);

        return $.ajax(req);
    }

    function del(url) {
        let req = makeRequest('DELETE', url);
        return $.ajax(req);
    }

    return {
        get,
        post,
        update,
        del
    }
});