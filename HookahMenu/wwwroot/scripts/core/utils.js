define(function () {
    const commonTemplates = {
        'header': './temp/common/header.hbs',
        'footer': './temp/common/footer.hbs'
    };

    function loadPage(ctx, templates) {
        Object.assign(templates, commonTemplates);

        ctx.loadPartials(templates).then(function () {
            this.partial(`./temp/common/main.hbs`);
        });
    }

    function showInfo(message) {
        $('#infoBox').find("span").text(message);
        $('#infoBox').show();
        setTimeout(() => $('#infoBox').fadeOut(), 3000);
    };

    function showError(ex) {
        $('#errorBox').find("span").text(ex.responseJSON.message);
        $('#errorBox').show();    
        setTimeout(() => $('#errorBox').fadeOut(), 3000);
    };

    return {
        loadPage,
        showInfo,
        showError
    }
});