﻿using System.Runtime.InteropServices.ComTypes;
using System.Threading.Tasks;
using HookahMenu.Data;
using HookahMenu.Data.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Facebook;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using HookahMenu.Models;
using HookahMenu.Services;
using HookahMenu.Services.Interfaces;
using Microsoft.AspNetCore.Identity;

namespace HookahMenu
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see https://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets<Startup>();
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddDbContext<HookahMenuContext>(options =>
                options.UseSqlServer("HookahMenu"));

            services.AddIdentity<Client, IdentityRole>()
                .AddEntityFrameworkStores<HookahMenuContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 5;
                options.Password.RequireLowercase = false;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ReferenceLoopHandling =
                    Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            });

            services.AddTransient<IDbContext, HookahMenuContext>();
            services.AddTransient<IHookahMenuData, HookahMenuData>();

            services.AddTransient<IOrdersService, OrdersService>();
            services.AddTransient<ITablesService, TablesService>();
            services.AddTransient<IMenuService, MenuService>();
            services.AddTransient<IUsersService, UsersService>();
        }

        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles(new StaticFileOptions()
            {
                ServeUnknownFileTypes = true
            });

            app.UseIdentity();


            app.UseFacebookAuthentication(new FacebookOptions()
            {
                AppId = "228591967672994",
                AppSecret = "59c8faffeaca8bd7a44416a7f5ba65a7"
            });

            app.UseMvc();

            using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetService<HookahMenuContext>();
                context.Database.EnsureCreated();
                await context.EnsureSeeded();
            }

            await CreateRoles();

            async Task CreateRoles()
            {
                var RoleManager = app.ApplicationServices.GetRequiredService<RoleManager<IdentityRole>>();
                var UserManager = app.ApplicationServices.GetRequiredService<UserManager<Client>>();

                string[] roleNames = { "Admin", "Member" };

                foreach (var roleName in roleNames)
                {
                    var roleExist = await RoleManager.RoleExistsAsync(roleName);
                    if (!roleExist)
                    {
                        await RoleManager.CreateAsync(new IdentityRole(roleName));
                    }
                }

                var poweruser = new Client()
                {
                    UserName = Configuration["AppSettings:UserName"]
                };

                string pass = Configuration["AppSettings:UserPassword"];
                var user = await UserManager.FindByNameAsync(Configuration["AppSettings:UserName"]);

                if (user != null)
                {
                    return;
                }

                var createPowerUser = await UserManager.CreateAsync(poweruser, pass);
                if (createPowerUser.Succeeded)
                {
                    await UserManager.AddToRoleAsync(poweruser, "Admin");

                }
            }
        }
    }
}
