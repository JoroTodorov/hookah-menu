﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using HookahMenu.Data;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;
using HookahMenu.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Services
{
    public class MenuService : Service, IMenuService
    {
        public MenuService(IHookahMenuData data) : base(data)
        {
        }

        public void OrderHookah(OrderDTO order, string clientId)
        {
            var client = this.Data.Clients.First(
                c => c.Id == clientId, c => c.Include(prop => prop.Visits));
            if (client == null)
            {
                throw new ArgumentException("No active user found.");
            }

            var visit = client.Visits.FirstOrDefault(v => v.IsActive);
            if (visit == null)
            {
                throw new ArgumentException("No active visit found.");
            }

            var tobaccos = this.Data.Tobaccos.Find(t => order.Tobaccos.Contains(t.Id)).ToArray();
            if (!tobaccos.Any())
            {
                throw new ArgumentException("You must choose at least one flavour for the shisha.");
            }

            var addons = this.Data.Addons.Find(a => order.Addons.Contains(a.Id)).ToArray();

            var orderAddons = addons.Select(a => new OrderAddon { Addon = a }).ToList();
            var orderTobaccos = tobaccos.Select(t => new OrderTobacco { Tobacco = t }).ToList();

            var newHookah = new Order()
            {
                Time = DateTime.Now,
                Tobaccos = orderTobaccos,
                Addons = orderAddons
            };

            visit.Orders.Add(newHookah);

            this.Data.Commit();
        }

        public object GetMenu()
        {
            var tobaccos = Mapper.Map<IEnumerable<TobaccoDTO>>(
                this.Data.Tobaccos.GetAll().OrderBy(t => t.Flavour));
            var addons = Mapper.Map<IEnumerable<AddonDTO>>(
                this.Data.Addons.GetAll().OrderBy(a => a.Type));

            var menu = new
            {
                Tobaccos = tobaccos,
                Addons = addons
            };

            return menu;
        }
    }
}