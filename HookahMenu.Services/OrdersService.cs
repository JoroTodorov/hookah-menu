﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using HookahMenu.Data;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;
using HookahMenu.Services.Interfaces;
using HookahMenu.Utilities;
using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Services
{
    public class OrdersService : Service, IOrdersService
    {
        public OrdersService(IHookahMenuData data) : base(data)
        {
        }

        public IEnumerable<OrderListDTO> GetRecentOrders(string clientId)
        {
            var client = this.Data.Clients.GetAll(c => c
                .Include(prop => prop.Visits)
                .ThenInclude(prop => prop.Orders)
                .ThenInclude(prop => prop.Addons).ThenInclude(o => o.Addon)
                .Include(prop => prop.Visits)
                .ThenInclude(prop => prop.Orders)
                .ThenInclude(prop => prop.Tobaccos).ThenInclude(prop => prop.Tobacco))
                .FirstOrDefault(c => c.Id == clientId);

            if (client == null)
            {
                throw new ArgumentException("No active user found.");
            }

            var currentVisit = client.Visits.FirstOrDefault(v => v.IsActive);
            if (currentVisit == null)
            {
                throw new ArgumentException("No active visit found.");
            }

            var orders = currentVisit.Orders.OrderByDescending(o => o.Time).Select(o => new OrderListDTO()
            {
                Price = o.CalculatePrice(),
                Tobaccos = Mapper.Map<ICollection<OrderTobaccoDTO>>(o.Tobaccos),
                Addons = Mapper.Map<ICollection<OrderAddonDTO>>(o.Addons)
            });

            return orders;
        }

        public void SetOrderAsFinished(int orderId)
        {
            var order = this.Data.Orders.First(o => o.Id == orderId, h => h.Include(a => a.Addons));
            if (order == null)
            {
                throw new ArgumentException("No order found.");
            }

            order.IsFinished = true;

            this.Data.Commit();
        }

        public void SetVisitAsPaid(int visitId)
        {
            var visit = this.Data.Visits.GetAll(v => v
                .Include(prop => prop.Orders)
                .ThenInclude(prop => prop.Tobaccos)
                .ThenInclude(prop => prop.Tobacco)
                .Include(prop => prop.Orders)
                .ThenInclude(prop => prop.Addons)
                .ThenInclude(prop => prop.Addon))
                .FirstOrDefault(v => v.Id == visitId);

            if (visit == null || !visit.IsActive)
            {
                throw new ArgumentException("Visit not found.");
            }

            if (visit.IsPaid)
            {
                throw new ArgumentException("Visit is already paid.");
            }

            visit.Price = visit.Orders.Select(o => o.CalculatePrice()).Sum();
            visit.IsPaid = true;
            visit.IsActive = false;

            this.Data.Commit();
        }
    }
}