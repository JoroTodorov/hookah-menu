﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper;
using HookahMenu.Data;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;
using HookahMenu.Services.Interfaces;
using HookahMenu.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Services
{
    public class TablesService : Service, ITablesService
    {
        public TablesService(IHookahMenuData data) : base(data)
        {
        }

        public IEnumerable<TableDTO> GetTables(Expression<Func<Table, bool>> exp)
        {
            var tables = this.Data.Tables.Find(exp);

            return Mapper.Map<IEnumerable<TableDTO>>(tables);
        }

        public TableVisitDTO GetTableOrders(int tableId)
        {
            var table = this.Data.Tables.First(t => t.Id == tableId,
                t => t.Include(prop => prop.Visits)
                    .ThenInclude(prop => prop.Orders)
                    .ThenInclude(prop => prop.Tobaccos)
                    .ThenInclude(prop => prop.Tobacco)
                    .Include(prop => prop.Visits)
                    .ThenInclude(prop => prop.Orders)
                    .ThenInclude(prop => prop.Addons)
                    .ThenInclude(prop => prop.Addon));

            if (table == null)
            {
                throw new ArgumentException("Table not found.");
            }

            var activeVisit = table.Visits.FirstOrDefault(v => v.IsActive);

            if (activeVisit == null)
            {
                throw new ArgumentException("No active visit on this table.");
            }

            var orders = activeVisit.Orders.Select(o => new TableOrdersDTO()
            {
                Id = o.Id,
                Price = o.CalculatePrice(),
                IsFinished = o.IsFinished,
                Tobaccos = Mapper.Map<ICollection<OrderTobaccoDTO>>(o.Tobaccos),
                Addons = Mapper.Map<ICollection<OrderAddonDTO>>(o.Addons)
            }).ToList(); 

            return new TableVisitDTO()
            {
                Id = activeVisit.Id,
                Orders = orders,
                Price = orders.Sum(o => o.Price)
            };
        }

        public void SetTableVisit(int tableId, string clientId)
        {
            var client = this.Data.Clients.First(
                c => c.Id == clientId, c => c.Include(prop => prop.Visits));
            if (client == null)
            {
                throw new ArgumentException("No user found.");
            }

            var table = this.Data.Tables.First(t => t.Id == tableId, t => t.Include(prop => prop.Visits));
            if (table == null || table.Visits.Any(v => v.IsActive))
            {
                throw new ArgumentException("Table is reserved or taken.");
            }

            var visit = new Visit()
            {
                Time = DateTime.Now,
                IsActive = true
            };

            var activeVisits = client.Visits.Where(v => v.IsActive);
            foreach (var clientVisit in activeVisits)
            {
                clientVisit.IsActive = false;
            }

            client.Visits.Add(visit);
            table.Visits.Add(visit);

            this.Data.Visits.Add(visit);

            this.Data.Commit();
        }

        public void FreeTable(int id)
        {
            var table = this.Data.Tables.First(t => t.Id == id, t => t.Include(prop => prop.Visits));
            if (table == null)
            {
                throw new ArgumentException("No table found.");
            }

            var visits = table.Visits.Where(v => v.IsActive);
            if (!visits.Any())
            {
                throw new ArgumentException("No active visits found.");
            }

            foreach (var visit in visits)
            {
                visit.IsActive = false;
            }

            this.Data.Commit();
        }

        public void AddTable()
        {
            var table = new Table();
            this.Data.Tables.Add(table);

            this.Data.Commit();
        }
    }
}