﻿using HookahMenu.Models;
using HookahMenu.Models.DTOs;

namespace HookahMenu.Services.Interfaces
{
    public interface IMenuService
    {
        void OrderHookah(OrderDTO order, string clientId);

        object GetMenu();
    }
}