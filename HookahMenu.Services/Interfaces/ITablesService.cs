﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;

namespace HookahMenu.Services.Interfaces
{
    public interface ITablesService
    {
        IEnumerable<TableDTO> GetTables(Expression<Func<Table, bool>> exp);

        TableVisitDTO GetTableOrders(int tableId);

        void SetTableVisit(int tableId, string clientId);

        void FreeTable(int id);

        void AddTable();
    }
}