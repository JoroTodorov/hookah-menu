﻿using HookahMenu.Data;

namespace HookahMenu.Services.Interfaces
{
    public interface IService
    {
        IHookahMenuData Data { get; set; }
    }
}