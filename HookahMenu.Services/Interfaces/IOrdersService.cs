﻿using System.Collections.Generic;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;

namespace HookahMenu.Services.Interfaces
{
    public interface IOrdersService
    {
        IEnumerable<OrderListDTO> GetRecentOrders(string clientId);

        void SetOrderAsFinished(int orderId); 

        void SetVisitAsPaid(int visitId); 
    }
}