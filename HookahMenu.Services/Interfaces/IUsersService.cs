﻿using System.Collections.Generic;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;

namespace HookahMenu.Services.Interfaces
{
    public interface IUsersService
    {
        VisitDTO GetActiveVisit(Client client);
    }
}