﻿using HookahMenu.Data;
using HookahMenu.Services.Interfaces;

namespace HookahMenu.Services
{
    public class Service : IService
    {
        public Service(IHookahMenuData data)
        {
            this.Data = data;
        }

        public IHookahMenuData Data { get; set; }
    }
}