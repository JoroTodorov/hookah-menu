﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using HookahMenu.Data;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;
using HookahMenu.Services.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Services
{
    public class UsersService : Service, IUsersService
    {
        public UsersService(IHookahMenuData data) : base(data)
        {
        }

        public VisitDTO GetActiveVisit(Client client)
        {
            var user = this.Data.Clients.First(
                u => u.Id == client.Id, c => c.Include(prop => prop.Visits));
            if (user == null)
            {
                throw new ArgumentException("No user found.");
            }

            var activeVisit = user.Visits.FirstOrDefault(v => v.IsActive);

            return activeVisit == null ? null : Mapper.Map<VisitDTO>(activeVisit);
        }
    }
}