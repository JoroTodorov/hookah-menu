﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HookahMenu.Models
{
    public class Addon
    {
        public Addon()
        {
            this.Orders = new HashSet<OrderAddon>();
        }

        [Key]
        public int Id { get; set; }

        public string Type { get; set; }

        public decimal Price { get; set; }

        public ICollection<OrderAddon> Orders { get; set; }
    }
}