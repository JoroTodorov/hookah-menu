﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace HookahMenu.Models
{
    public class Client : IdentityUser
    {
        public Client()
        {
            this.Visits = new HashSet<Visit>();
        }

        public bool IsAdmin { get; set; }

        public ICollection<Visit> Visits { get; set; }
    }
}
