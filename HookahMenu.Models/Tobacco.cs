﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HookahMenu.Models
{
    public class Tobacco
    {
        public Tobacco()
        {
            this.Orders = new HashSet<OrderTobacco>();
        }

        [Key]
        public int Id { get; set; }

        public string Flavour { get; set; }

        public ICollection<OrderTobacco> Orders { get; set; }
    }
}