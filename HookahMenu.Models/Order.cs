﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HookahMenu.Models
{
    public class Order
    {
        private const decimal DefaultShishaPrice = 15;

        public Order()
        {
            this.Tobaccos = new HashSet<OrderTobacco>();
            this.Addons = new HashSet<OrderAddon>();

            this.Price = DefaultShishaPrice;
            this.IsFinished = false;
        }

        [Key]
        public int Id { get; set; }

        public decimal Price { get; set; }

        public bool IsFinished { get; set; }

        public DateTime Time { get; set; }

        public Visit Visit { get; set; }

        public ICollection<OrderTobacco> Tobaccos { get; set; }

        public ICollection<OrderAddon> Addons { get; set; }
    }
}