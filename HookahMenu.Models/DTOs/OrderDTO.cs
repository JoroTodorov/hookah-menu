﻿using System.Collections.Generic;

namespace HookahMenu.Models.DTOs
{
    public class OrderDTO
    {
        public IEnumerable<int> Tobaccos { get; set; }

        public IEnumerable<int> Addons { get; set; }
    }
}
