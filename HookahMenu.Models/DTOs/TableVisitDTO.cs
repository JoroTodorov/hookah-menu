﻿using System.Collections.Generic;

namespace HookahMenu.Models.DTOs
{
    public class TableVisitDTO
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public IEnumerable<TableOrdersDTO> Orders { get; set; }
    }
}