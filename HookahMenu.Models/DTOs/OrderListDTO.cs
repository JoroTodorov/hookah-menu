﻿using System.Collections.Generic;

namespace HookahMenu.Models.DTOs
{
    public class OrderListDTO
    {
        public decimal Price { get; set; }

        public ICollection<OrderAddonDTO> Addons { get; set; }

        public ICollection<OrderTobaccoDTO> Tobaccos { get; set; }
    }
}