﻿using System.Collections.Generic;

namespace HookahMenu.Models.DTOs
{
    public class TableOrdersDTO
    {
        public int Id { get; set; }

        public decimal Price { get; set; }

        public bool IsFinished { get; set; }

        public ICollection<OrderAddonDTO> Addons { get; set; }

        public ICollection<OrderTobaccoDTO> Tobaccos { get; set; }
    }
}