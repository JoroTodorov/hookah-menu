﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HookahMenu.Models
{
    public class Table
    {
        public Table()
        {
            this.Visits = new HashSet<Visit>();
        }

        [Key]
        public int Id { get; set; }

        public ICollection<Visit> Visits { get; set; }
    }
}