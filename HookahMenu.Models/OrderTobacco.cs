﻿using System.ComponentModel.DataAnnotations;

namespace HookahMenu.Models
{
    public class OrderTobacco
    {
        public int OrderId { get; set; }
        public Order Order { get; set; }

        public int TobaccoId { get; set; }
        public Tobacco Tobacco { get; set; }
    }
}