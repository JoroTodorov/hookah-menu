﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HookahMenu.Models
{
    public class Visit
    {
        public Visit()
        {
            this.Orders = new HashSet<Order>();
        }

        [Key]
        public int Id { get; set; }

        public bool IsActive { get; set; }

        public bool IsPaid { get; set; }

        public decimal Price { get; set; }

        public DateTime Time { get; set; }

        public Table Table { get; set; }

        public Client Client { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}