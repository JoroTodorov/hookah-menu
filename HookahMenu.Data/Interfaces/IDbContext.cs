﻿using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Data.Interfaces
{
    public interface IDbContext
    {
        DbSet<T> Set<T>() where T : class;

        int SaveChanges();

        void Dispose();
    }
}