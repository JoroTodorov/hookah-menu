﻿using System;
using System.Linq;
using System.Linq.Expressions;
using HookahMenu.Models;
using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Data.Interfaces
{
    public interface IRepository<T> where T : class
    {
        T[] ToArray { get; }

        int Count { get; }

        void Add(T entity);

        void AddRange(IQueryable<T> entities);

        void Remove(T entity);

        void RemoveRange(IQueryable<T> entities);

        T GetById(object id);

        IQueryable<T> GetAll(Func<IQueryable<T>, IQueryable<T>> actions = null);

        T First(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> actions = null);

        IQueryable<T> Find(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> actions = null);
    }
}