﻿using HookahMenu.Data.Interfaces;
using HookahMenu.Models;

namespace HookahMenu.Data
{
    public interface IHookahMenuData
    {
        IRepository<Addon> Addons { get; }

        IRepository<Table> Tables { get; }

        IRepository<Tobacco> Tobaccos { get; }

        IRepository<Order> Orders { get; }

        IRepository<OrderTobacco> OrderTobaccos { get; }

        IRepository<OrderAddon> OrderAddons { get; }

        IRepository<Visit> Visits { get; }

        IRepository<Client> Clients { get; }

        void Commit();
    }
}