﻿using HookahMenu.Data.Interfaces;
using HookahMenu.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace HookahMenu.Data
{
    public class HookahMenuContext : IdentityDbContext<Client>, IDbContext
    {
        public HookahMenuContext(DbContextOptions<HookahMenuContext> options)
            : base(options)
        {
        }

        public HookahMenuContext()
        {

        }

        public DbSet<Addon> Addons { get; set; }

        public DbSet<Table> Tables { get; set; }

        public DbSet<Tobacco> Tobaccos { get; set; }

        public DbSet<Order> Orders { get; set; }

        public DbSet<Client> Clients { get; set; }

        public DbSet<Visit> Visits { get; set; }

        public DbSet<OrderAddon> OrderAddons { get; set; }

        public DbSet<OrderTobacco> OrderTobaccos { get; set; }
        

        public new DbSet<T> Set<T>() where T : class
        {
            return base.Set<T>();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderAddon>()
                .HasKey(h => new {HookahId = h.OrderId, h.AddonId});

            modelBuilder.Entity<OrderTobacco>()
                .HasKey(h => new { HookahId = h.OrderId, h.TobaccoId });

            modelBuilder.Entity<Tobacco>()
                .HasMany(t => t.Orders)
                .WithOne(h => h.Tobacco);

            modelBuilder.Entity<Addon>()
                .HasMany(h => h.Orders)
                .WithOne(h => h.Addon);

            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(
                @"Server=.;Database=HookahMenu;Trusted_Connection=True;MultipleActiveResultSets=true");
        }
    }
}