﻿
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HookahMenu.Models;
using HookahMenu.Utilities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Newtonsoft.Json;

namespace HookahMenu.Data
{
    public static class HookahMenuContextExtension
    {
        private const string DefaultLinkPath = "http://localhost:3000/content";

        public static async Task EnsureSeeded(this HookahMenuContext context)
        {
            if (!context.Tobaccos.Any())
            {
                var tobaccos = await GetContent<Tobacco>();
                foreach (var tobacco in tobaccos)
                {
                    context.Tobaccos.Add(tobacco);
                }
            }

            if (!context.Addons.Any())
            {
                var addons = await GetContent<Addon>();

                foreach (var addon in addons)
                {
                    context.Addons.Add(addon);
                }
            }

            if (!context.Tables.Any())
            {
                for (int num = 0; num < 6; num++)
                {
                    var table = new Table();
                    context.Tables.Add(table);
                }
            }

            context.SaveChanges();

            async Task<IEnumerable<T>> GetContent<T>()
            {
                string fileName = typeof(T).Name.ToLower();
                var request = WebRequest.CreateHttp($"{DefaultLinkPath}/{fileName}s.json");

                string json = null;

                using (var response = await request.GetResponseAsync())
                {
                    var responseStream = response.GetResponseStream();
                    if (responseStream != null)
                    {
                        var reader = new StreamReader(responseStream);
                        json = reader.ReadToEnd();
                        reader.Dispose();
                    }
                }

                var objects = JsonConvert.DeserializeObject<IEnumerable<T>>(json);

                return objects;
            }
        }
    }
}