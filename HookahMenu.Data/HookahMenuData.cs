﻿using HookahMenu.Data.Interfaces;
using HookahMenu.Models;

namespace HookahMenu.Data
{
    public class HookahMenuData : IHookahMenuData
    {
        private readonly IDbContext context;

        private IRepository<Addon> addons;

        private IRepository<Table> tables;

        private IRepository<Tobacco> tobaccos;

        private IRepository<Order> orders;

        private IRepository<OrderTobacco> orderTobaccos;

        private IRepository<OrderAddon> orderAddons;

        private IRepository<Visit> visits;

        private IRepository<Client> clients;

        public HookahMenuData(IDbContext context)
        {
            this.context = context;
        }

        public IRepository<Addon> Addons => this.addons ?? (this.addons = new Repository<Addon>(this.context));

        public IRepository<Table> Tables => this.tables ?? (this.tables = new Repository<Table>(this.context));

        public IRepository<Tobacco> Tobaccos => this.tobaccos ?? (this.tobaccos = new Repository<Tobacco>(this.context));

        public IRepository<Order> Orders => this.orders ?? (this.orders = new Repository<Order>(this.context));

        public IRepository<OrderTobacco> OrderTobaccos => this.orderTobaccos ?? (this.orderTobaccos = new Repository<OrderTobacco>(this.context));

        public IRepository<OrderAddon> OrderAddons => this.orderAddons ?? (this.orderAddons = new Repository<OrderAddon>(this.context));

        public IRepository<Visit> Visits => this.visits ?? (this.visits = new Repository<Visit>(this.context));

        public IRepository<Client> Clients => this.clients ?? (this.clients = new Repository<Client>(this.context));

        public void Commit()
        {
            this.context.SaveChanges();
        }
    }
}