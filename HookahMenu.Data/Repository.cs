﻿using System;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper.Execution;
using HookahMenu.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace HookahMenu.Data.Interfaces
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private readonly DbSet<T> modelTable;

        public Repository(IDbContext context)
        {
            this.modelTable = context.Set<T>();
        }

        public T[] ToArray => this.GetAll().ToArray();

        public int Count => this.GetAll().Count();

        public void Add(T entity)
        {
            this.modelTable.Add(entity);
        }

        public void AddRange(IQueryable<T> entities)
        {
            foreach (var entity in entities)
            {
                this.modelTable.Add(entity);
            }
        }

        public void Remove(T entity)
        {
            this.modelTable.Remove(entity);
        }

        public void RemoveRange(IQueryable<T> entities)
        {
            this.modelTable.RemoveRange(entities);
        }

        public T GetById(object id)
        {
            return this.modelTable.Find(id);
        }

        public IQueryable<T> GetAll(Func<IQueryable<T>, IQueryable<T>> actions = null)
        {
            var entities = actions == null ?
                this.modelTable : actions(this.modelTable);

            return entities;
        }

        public T First(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> actions = null)
        {
            return this.GetAll(actions).FirstOrDefault(predicate);
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate,
            Func<IQueryable<T>, IQueryable<T>> actions = null)
        {
            return this.GetAll(actions).Where(predicate);
        }
    }
}