﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;
using HookahMenu.Models;
using HookahMenu.Models.DTOs;

namespace HookahMenu.Utilities
{
    public class MapperConfig
    {
        public static void Initialize()
        {
            Mapper.Initialize(m =>
            {
                m.CreateMap<Addon, AddonDTO>();
                m.CreateMap<Tobacco, TobaccoDTO>();
                m.CreateMap<Table, TableDTO>();

                m.CreateMap<Visit, VisitDTO>().ForMember(dto => dto.TableId,
                    model => model.MapFrom(prop => prop.Table.Id));

                m.CreateMap<OrderAddon, OrderAddonDTO>()
                    .ForMember(dto => dto.Type,
                        model => model.MapFrom(prop => prop.Addon.Type));

                m.CreateMap<OrderTobacco, OrderTobaccoDTO>()
                    .ForMember(dto => dto.Flavour,
                        model => model.MapFrom(prop => prop.Tobacco.Flavour));
            });
        }
    }
}
