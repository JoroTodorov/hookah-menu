﻿using System.Linq;
using HookahMenu.Models;

namespace HookahMenu.Utilities
{
    public static class OrderExtensions
    {
        public static decimal CalculatePrice(this Order order)
        {
            return order.Price + order.Addons.Sum(a => a.Addon.Price);
        }
    }
}